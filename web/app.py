from flask import Flask, render_template, request
import os
import re

app = Flask(__name__)

@app.route("/")
def index():
    return "<h1>Welcome to the Homepage!</h1><style>h1{color:purple;text-align:center;}</style>"


@app.route("/<request>", methods=['GET', 'POST'])
def give_data(request):
    is_forbidden = re.search("[.]{2}|[\~]|[/]", request)
    if is_forbidden:
        return render_template("./403.html")
    elif os.path.exists("./templates/" + request):
            return "<h1>200/OK</h1><style>h1{color:green;}</style>" + render_template("./" + request)
    else:
        return render_template("./404.html")

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
